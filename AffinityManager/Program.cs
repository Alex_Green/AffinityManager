﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace AffinityManager
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length >= 2 && args[0].Length == 2)
            {
                char ArgsKey = args[0].ToLowerInvariant()[1];
                if (ArgsKey == 'n' || ArgsKey == 'i')
                {
                    Process[] Processes = new Process[0];
                    if (ArgsKey == 'n')
                    {
                        Processes = Process.GetProcessesByName(args[1]);
                        if (Processes.Length == 0)
                        {
                            Console.WriteLine("No processes with Name: \"{0}\" found.", args[1]);
                            return;
                        }
                    }
                    else if (ArgsKey == 'i')
                    {
                        int ProcessID = 0;
                        if (!int.TryParse(args[1], out ProcessID))
                        {
                            Console.WriteLine("Process ID parse error.");
                            return;
                        }

                        try
                        {
                            Processes = new Process[] { Process.GetProcessById(ProcessID) };
                        }
                        catch
                        {
                            Console.WriteLine("No processes with ID: \"{0}\" found.", args[1]);
                            return;
                        }
                    }

                    if (args.Length == 2)
                    {
                        for (int i = 0; i < Processes.Length; i++)
                            Console.WriteLine(Processes[i].ProcessorAffinity);
                    }
                    else
                    {
                        IntPtr Affinity = ParseAffinity(args[2]);
                        for (int i = 0; i < Processes.Length; i++)
                        {
                            try
                            {
                                Processes[i].ProcessorAffinity = Affinity;
                                Console.WriteLine("Process Name: {0}", Processes[i].ProcessName);
                                Console.WriteLine("  Process ID: {0}", Processes[i].Id);
                                Console.WriteLine("    Affinity: {0}", Processes[i].ProcessorAffinity);
                                Console.WriteLine();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }
                    }
                }
                else if (ArgsKey == 'f') // -f or -F
                {
                    if (args.Length < 3)
                    {
                        Console.WriteLine("Need more args.");
                        return;
                    }

                    Process P = new Process();
                    P.StartInfo.FileName = args[1];
                    ProcessPriorityClass PriorityClass = ProcessPriorityClass.Normal;
                    int ArgumentsIndex = 0;

                    if (args.Length > 3)
                    {
                        for (int i = 3; i < args.Length; i++)
                            if (args[i].Length == 2 && args[i].ToLowerInvariant()[1] == 'a')
                                ArgumentsIndex = i;
                        int LastIndex = (ArgumentsIndex == 0 ? args.Length : ArgumentsIndex);

                        for (int i = 3; i < LastIndex; i++)
                        {
                            if (args[i].Length == 2 && args[i].ToLowerInvariant()[1] == 'p' && i + 1 < args.Length)
                                try
                                {
                                    PriorityClass = (ProcessPriorityClass)Enum.Parse(typeof(ProcessPriorityClass), args[i + 1], true);
                                    i++;
                                }
                                catch { }

                            if (args[i].Length == 2 && args[i].ToLowerInvariant()[1] == 'w' && i + 1 < args.Length)
                                try
                                {
                                    P.StartInfo.WindowStyle = (ProcessWindowStyle)Enum.Parse(typeof(ProcessWindowStyle), args[i + 1], true);
                                    i++;
                                }
                                catch { }

                            bool CreateNoWindow = false;
                            if (args[i].Length == 2 && args[i].ToLowerInvariant()[1] == 'n' && i + 1 < args.Length && bool.TryParse(args[i + 1], out CreateNoWindow))
                            {
                                P.StartInfo.CreateNoWindow = CreateNoWindow;
                                i++;
                            }

                            bool UseShellExecute = false;
                            if (args[i].Length == 2 && args[i].ToLowerInvariant()[1] == 's' && i + 1 < args.Length && bool.TryParse(args[i + 1], out UseShellExecute))
                            {
                                P.StartInfo.UseShellExecute = UseShellExecute;
                                i++;
                            }

                            if (args[i].Length == 2 && args[i].ToLowerInvariant()[1] == 'd' && i + 1 < args.Length)
                            {
                                P.StartInfo.WorkingDirectory = args[i + 1];
                                i++;
                            }
                        }

                        if (ArgumentsIndex != 0)
                        {
                            string Arguments = String.Empty;
                            for (int i = ArgumentsIndex + 1; i < args.Length; i++)
                                Arguments += args[i] + ' ';
                            P.StartInfo.Arguments = Arguments;
                        }
                    }

                    try
                    {
                        P.Start();
                    }
                    catch
                    {
                        Console.WriteLine("Can't start process. File \"{0}\" does not exists.", args[1]);
                        return;
                    }
                    P.PriorityClass = PriorityClass;
                    P.ProcessorAffinity = ParseAffinity(args[2]);
                    P.Dispose();
                }
                else
                {
                    Console.WriteLine("Don't can parse first arg.");
                }
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Affinity Manager Usage:\n");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("AffinityManager -N ProcessName [Affinity]");
                Console.WriteLine("AffinityManager -I ProcessID [Affinity]");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("AffinityManager -F Path Affinity [Params]");
                Console.ResetColor();
                Console.WriteLine("    Affinity - value from bitmask. Example:");
                Console.WriteLine("        If Affinity is not used, program will return process affinity value.");
                Console.WriteLine("        For Core0 and Core2: Bitmask is 0b101 (HEX: 0h5, decimal: 5).");
                Console.WriteLine("        For using All cores value must be less or equal 0.");
                Console.WriteLine("    Path - path to executable file for starting.");
                Console.WriteLine("    Params:");
                Console.WriteLine("        -P - Priority: Idle, BelowNormal, Normal, AboveNormal, High, RealTime.");
                Console.WriteLine("        -W - Window style: Normal, Minimized, Maximized, Hidden.");
                Console.WriteLine("             To use Hidden, the UseShellExecute property must be false!");
                Console.WriteLine("        -N - NoWindow: indicating whether to start the process in a new window.");
                Console.WriteLine("        -S - UseShellExecute: whether to use the OS shell to start the process.");
                Console.WriteLine("             \"true\" if the shell should be used when starting the process;");
                Console.WriteLine("             \"false\" if the process should be created directly from the");
                Console.WriteLine("             executable file. The default is true.");
                Console.WriteLine("        -D - Working Directory: When the UseShellExecute is false, sets the");
                Console.WriteLine("             working directory for the process to be started. When true - sets");
                Console.WriteLine("             the directory that contains the process to be started.");
                Console.WriteLine("        -A - Command-line arguments to use when starting the application.\n");
            }
        }

        static IntPtr ParseAffinity(string AffinityStr)
        {
            int MaxAffinity = (1 << Environment.ProcessorCount) - 1;
            int Affinity = -1;

            if (!int.TryParse(AffinityStr, out Affinity))
                Console.WriteLine("Don't can parse Affinity.");

            if (Affinity <= 0 || Affinity > MaxAffinity)
                Affinity = MaxAffinity;

            return (IntPtr)Affinity;
        }
    }
}
