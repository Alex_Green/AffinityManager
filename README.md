# AffinityManager

## Description
 AffinityManager is a command-line open source C# program (.NET 2.0, written only on managed code).
 It can gets or sets affinity for processes by ID or Name.
 Also you can launch any process with some affinity value and other parameters.

## Download
### [Precompiled version for Windows (AnyCPU)](Precompiled/AffinityManager.exe)

## License:
**MIT**

## Usage:
	AffinityManager -N ProcessName [Affinity]
	AffinityManager -I ProcessID [Affinity]
	AffinityManager -F Path Affinity [Params]
		Affinity - value from bitmask. Example:
			If Affinity is not used, program will return process affinity value.
			For Core0 and Core2: Bitmask is 0b101 (HEX: 0h5, decimal: 5).
			For using All cores value must be less or equal 0.
		Path - path to executable file for starting.
		Params:
			-P - Priority: Idle, BelowNormal, Normal, AboveNormal, High, RealTime.
			-W - Window style: Normal, Minimized, Maximized, Hidden.
				 To use Hidden, the UseShellExecute property must be false!
			-N - NoWindow: indicating whether to start the process in a new window.
			-S - UseShellExecute: whether to use the OS shell to start the process.
				 "true" if the shell should be used when starting the process;
				 "false" if the process should be created directly from the
				 executable file. The default is true.
			-D - Working Directory: When the UseShellExecute is false, sets the
				 working directory for the process to be started. When true - sets
				 the directory that contains the process to be started.
			-A - Command-line arguments to use when starting the application.

## Author
Zelenskyi Alexandr (Зеленский Александр) - alex.green.zaa.93@gmail.com